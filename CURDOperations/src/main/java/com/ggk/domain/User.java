package com.ggk.domain;

public class User {
	private int uid;
	private String fname;
	private String lname;
	private String loc;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(int uid, String fname, String lname, String loc) {
		this.uid = uid;
		this.fname = fname;
		this.lname = lname;
		this.loc = loc;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", fname=" + fname + ", lname=" + lname + ", loc=" + loc + "]";
	}

}
