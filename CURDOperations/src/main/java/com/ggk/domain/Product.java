package com.ggk.domain;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity(label = "Product")
public class Product {
	@org.neo4j.ogm.annotation.Id
	@GeneratedValue
	private Long Id;

	private int pid;
	private String name;
	private String manufacture;
	private double price;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [Id=" + Id + ", pid=" + pid + ", name=" + name + ", manufacture=" + manufacture + ", price="
				+ price + "]";
	}

}
