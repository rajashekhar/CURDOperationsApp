package com.ggk.repositories;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import com.ggk.domain.Product;

public interface ProductRepositories extends Neo4jRepository<Product, Long> {

}
