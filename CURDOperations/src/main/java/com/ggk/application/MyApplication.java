package com.ggk.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ggk.controller")
public class MyApplication {
	public static void main(String[] args) {
		System.out.println("This is a my first curd operation...");
		SpringApplication.run(MyApplication.class, args);
		System.out.println("This is added from git ");
	}
}
