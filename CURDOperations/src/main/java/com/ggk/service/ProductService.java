package com.ggk.service;

import java.util.List;

import com.ggk.domain.Product;

public interface ProductService {

	List<Product> getAll();

	Product getById(Long id);

	Product saveOrUpdate(Product product);

	String delete(Product product);

	Product getByName(String name);
}
