package com.ggk.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ggk.domain.Product;
import com.ggk.repositories.ProductRepositories;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepositories productRepository;

	@Override
	public List<Product> getAll() {
		List<Product> products = new ArrayList<Product>();
		productRepository.findAll().forEach(products::add);
		System.out.println("Products ::::;::::: " + products);
		return products;
	}

	@Override
	public Product getById(Long id) {
		return productRepository.findById(id).orElse(null);
	}

	@Override
	public Product saveOrUpdate(Product product) {
		return productRepository.save(product);
	}

	@Override
	public String delete(Product product) {
		productRepository.delete(product);
		return "Product Deleted Succeddfully...." + product.getId();
	}

	@Override
	public Product getByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
