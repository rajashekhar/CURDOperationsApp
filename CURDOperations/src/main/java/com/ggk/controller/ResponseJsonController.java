package com.ggk.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ggk.domain.User;
import com.ggk.service.UserServie;

@RestController
@RequestMapping("/")
public class ResponseJsonController {

	// @Autowired
	// private UserServie userServie;

	@GetMapping("/all")
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		User user1 = new User(1001, "JOHN", "MILLER", "US");
		User user2 = new User(1002, "SCOTT", "JOHN", "DOLLES");
		User user3 = new User(1003, "RAMA", "KRISHNA", "US");
		User user4 = new User(1004, "KUMAR", "SAI", "US");
		User user5 = new User(1005, "JOHN", "MILLER", "US");
		users.add(user1);
		users.add(user2);
		users.add(user3);
		users.add(user4);
		users.add(user5);
		return users;
	}

	@GetMapping("/wish")
	public String wishMsg() {
		return "Hai....,How r u";
	}

	@PostMapping("/save")
	public User addNewOne(@RequestBody User user) {
		System.out.println("User Data is :::::::::::" + user);
		user.setLoc(user.getFname() + "_" + user.getLname());
		return user;
	}
}
