package com.ggk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ggk.domain.Product;
import com.ggk.domain.User;
import com.ggk.service.ProductService;

//@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/")
	public String redirectToList() {
		return "redirect:/product/list";
	}

	@PostMapping("/save")
	public Product createProduct(Product product) {
		
		return productService.saveOrUpdate(product);
	}


}
